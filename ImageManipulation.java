import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class ImageManipulation extends JFrame implements ActionListener {

    private JLabel imageLabel;
    private JPanel btnPanel;
    private JPanel radioBtnBrightnessPanel;
    private JPanel brightnessPanel;
    private ButtonGroup radioBtnGroup;
    private JRadioButton btnBrightness20;
    private JRadioButton btnBrightness50;
    private JRadioButton btnBrightness70;
    private JButton btnImageSelect;
    private JButton btnBrightness;
    private JButton btnBinaryImage;
    private JButton btnSmoothing;
    private JButton btnReset;
    private JFileChooser imageChooser;
    private BufferedImage image;
    private int[][] originalRGB;

    public ImageManipulation() {
        initUI();
    }

    private void initUI() {
        // Create necessary buttons and radio buttons
        btnImageSelect = new JButton("Select Image");
        btnBrightness20 = new JRadioButton("20");
        btnBrightness50 = new JRadioButton("50");
        btnBrightness70 = new JRadioButton("70");
        btnBrightness = new JButton("Apply brightness");
        btnBinaryImage = new JButton("Make image binary");
        btnSmoothing = new JButton("Apply smoothing");
        btnReset = new JButton("Reset image");

        // Add brightness radio buttons to group (for mutual exclusivity)
        radioBtnGroup = new ButtonGroup();
        radioBtnGroup.add(btnBrightness20);
        radioBtnGroup.add(btnBrightness50);
        radioBtnGroup.add(btnBrightness70);

        // Add brightness radio buttons to panel
        radioBtnBrightnessPanel = new JPanel(new FlowLayout());
        radioBtnBrightnessPanel.add(btnBrightness20);
        radioBtnBrightnessPanel.add(btnBrightness50);
        radioBtnBrightnessPanel.add(btnBrightness70);

        // Add brightness related buttons to common panel
        brightnessPanel = new JPanel();
        BoxLayout brightnessLayout = new BoxLayout(brightnessPanel, BoxLayout.Y_AXIS);
        brightnessPanel.setLayout(brightnessLayout);
        brightnessPanel.add(radioBtnBrightnessPanel);
        brightnessPanel.add(btnBrightness);
        btnBrightness.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Add all buttons to button panel
        btnPanel = new JPanel(new FlowLayout());
        btnPanel.add(btnImageSelect);
        btnPanel.add(brightnessPanel);
        btnPanel.add(btnBinaryImage);
        btnPanel.add(btnSmoothing);
        btnPanel.add(btnReset);

        // Get image file button
        btnImageSelect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Create a file chooser and add filetype filter to it
                imageChooser = new JFileChooser();
                imageChooser.addChoosableFileFilter(new ImageFilter());
                imageChooser.setAcceptAllFileFilterUsed(false);
                int option = imageChooser.showSaveDialog(new JFrame());

                // Select file, get the buffered image -> ImageIcon and add it to window
                if (option == JFileChooser.APPROVE_OPTION) {
                    // Remove old image if present
                    if (imageLabel != null) {  
                        imageLabel.removeAll();
                        getContentPane().remove(imageLabel);
                    }
                    // Get image file and add to label
                    File imageFile = imageChooser.getSelectedFile();
                    loadImage(imageFile);
                    imageLabel = new JLabel(new ImageIcon(image));
                    getContentPane().add(imageLabel,BorderLayout.CENTER);
                    pack();
                    
                    initOriginalRGB();  

                    // Set buttons enabled
                    btnBrightness20.setEnabled(true);
                    btnBrightness50.setEnabled(true);
                    btnBrightness70.setEnabled(true);
                    btnBinaryImage.setEnabled(true);
                    btnSmoothing.setEnabled(true);
                    btnReset.setEnabled(true);
                }
            }
        });

        // Brightness radio button pressed -> enable brightness application button
        btnBrightness20.setActionCommand("BRIGHTNESS");
        btnBrightness50.setActionCommand("BRIGHTNESS");
        btnBrightness70.setActionCommand("BRIGHTNESS");
        btnBrightness20.addActionListener((ActionListener) this);
        btnBrightness50.addActionListener((ActionListener) this);
        btnBrightness70.addActionListener((ActionListener) this);


        // Return to original button
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                returnToOriginal();
            }
        });

        // Set binary image button
        btnBinaryImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                turnBinary();
            }
        });

        // Set brightness button
        btnBrightness.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeBrightness();
            }
        });

        // Set smoothness button
        btnSmoothing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                smoothImage();
            }
        });

        // Diable buttons until picture is selected
        btnBrightness.setEnabled(false);
        btnBrightness20.setEnabled(false);
        btnBrightness50.setEnabled(false);
        btnBrightness70.setEnabled(false);
        btnBinaryImage.setEnabled(false);
        btnSmoothing.setEnabled(false);
        btnReset.setEnabled(false);
    
        // Set up window
        setSize(1200, 900);
        getContentPane().add(btnPanel, BorderLayout.NORTH);
        pack();
        setTitle("Image manipulation");
        setVisible(true);
    }

    // Load file -> BufferedImage
    private void loadImage(File imageFile) {
        image = null;

        try {
            image = ImageIO.read(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Saving original values, to be able to reset
    private void initOriginalRGB(){
        int width = this.image.getWidth();
        int height = this.image.getHeight();
        this.originalRGB = new int[width][height];

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                this.originalRGB[x][y]= this.image.getRGB(x,y);
            }
        }
    }

    // Return image to original state
    private void returnToOriginal() {
        int width = this.image.getWidth();
        int height = this.image.getHeight();

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                this.image.setRGB(x, y, this.originalRGB[x][y]);
            }
        }
        repaint();
    }

    // Make image binary
    private void turnBinary() {
        int width = this.image.getWidth();
        int height = this.image.getHeight(); 

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                // Get current pixel values
                int pixel = this.image.getRGB(x, y);
                int red = (pixel>>16)&0xff;
                int green = (pixel>>8)&0xff;
                int blue = pixel&0xff;

                // Calculate avg
                int avg = (red + green + blue)/3;

                // Set new value based on avg
                if (avg > 150) {
                    this.image.setRGB(x, y, 0xffffff);
                } else {
                    this.image.setRGB(x, y, 0);
                }
            }
        }
        repaint();
    }

    // Check which brightness level is chosen
    private void changeBrightness() {
        if (btnBrightness20.isSelected()) {
            brightnessChanger(20);
        } else if (btnBrightness50.isSelected()) {
            brightnessChanger(50);
        } else {
            brightnessChanger(70);
        }
    }

    // Applies new brightness to image
    private void brightnessChanger(int brightness) {
        int width = this.image.getWidth();
        int height = this.image.getHeight(); 

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                // Get current pixel values
                int pixel = this.image.getRGB(x, y);
                int red = (pixel>>16)&0xff;
                int green = (pixel>>8)&0xff;
                int blue = pixel&0xff;

                // Create new color and get color value
                Color newColor = new Color(brightnessChecker(red, brightness), brightnessChecker(green, brightness),brightnessChecker(blue, brightness));
                int newValue = newColor.getRGB();

                // Set new value
                this.image.setRGB(x, y, newValue);
            }
        }
        repaint();
    }

    // Checks that new value doesn't wrap over 255
    private int brightnessChecker(int currentValue, int increase) {
        if (currentValue + increase > 255) {
            return 255;
        } else {
            return currentValue + increase;
        }
    }

    private void smoothImage() {
        int width = this.image.getWidth();
        int height = this.image.getHeight(); 

        // Create new array to not smooth more as we get further
        int[][] newImage = new int[width][height];
        
        // Loop through picture pixels
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                List<Integer> currentValues = new LinkedList<Integer>();

                // Get neighbour pixel values
                for (int nRow = -1; nRow <= 1; nRow++) {
                    for (int nCol = -1; nCol <= 1; nCol++) {

                        // If an edge: skip
                        if (x + nRow < 0 ||
                            x + nRow >= width ||
                            y + nCol < 0 || 
                            y + nCol >= height)
                        {
                            continue;
                        }

                        // Add value to list
                        currentValues.add(this.image.getRGB(x+nRow, y+nCol));
                    }
                }

                // Sort list and find median value
                Collections.sort(currentValues);
                int medianPixel = median(currentValues);

                // Save median value pixel to new image array
                newImage[x][y] = medianPixel;
            }
        }
        
        // Construct new image from medianPixel array
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                this.image.setRGB(x, y, newImage[x][y]);
            }
        }
        
        repaint();
    }

    // Get median of a list, in integer
    private int median (List<Integer> neighbourhoodPixels) {
        int middle = neighbourhoodPixels.size()/2;
        double result;

        if (neighbourhoodPixels.size() % 2 == 1) {
            result = neighbourhoodPixels.get(middle);
        } else {
           result = (neighbourhoodPixels.get(middle-1) + neighbourhoodPixels.get(middle)) / 2.0;
        }
        return (int) Math.round(result);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Enables set button for applying brightness when a brightness level is chosen
        if (e.getActionCommand().equals("BRIGHTNESS")) {
            btnBrightness.setEnabled(true);
        }
    }

}